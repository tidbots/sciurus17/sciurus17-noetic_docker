# Copyright (c) 2024 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM nvidia/cudagl:11.3.0-devel-ubuntu20.04

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="July 2, 2024"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ARG ROS_DISTRO=noetic

# Timezone, Launguage Setting
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe

# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo

RUN apt-get update && apt-get install -q -y --no-install-recommends \
    wget build-essential gcc git vim \
    dirmngr vim sudo git\
    libosmesa6-dev wget iputils-ping net-tools \
    x11-utils x11-apps terminator xterm \
    lsb-release iproute2 gnupg gnupg2 gnupg1 \
    ca-certificates language-pack-ja-base locales fonts-takao \
    debconf-utils curl  python3-pip wget && \
    pip install -U --no-cache-dir supervisor supervisor_twiddler rosdep && \
    rm -rf /var/lib/apt/lists/*

# Install ROS1 noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt-get update && apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
libpcap-dev \
libopenblas-dev \
gstreamer1.0-tools libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev \
ros-noetic-desktop-full \
python3-rosdep python3-rosinstall-generator python3-vcstool build-essential \
ros-noetic-socketcan-bridge \
ros-noetic-geodesy \
python3-catkin-tools ros-noetic-gmapping ros-noetic-navigation \
python-is-python3 chrony \
python3-wstool ninja-build stow \
sshpass && \
apt-get clean && rm -rf /var/lib/apt/lists/*

RUN sudo rosdep init && rosdep update
RUN source /opt/ros/noetic/setup.bash && \
    mkdir -p /tmp/catkin_ws/src && cd /tmp/catkin_ws/src && \
    catkin_init_workspace && \
    git clone https://github.com/rt-net/sciurus17_ros.git && \
    git clone https://github.com/rt-net/sciurus17_description.git && \
    cd /tmp/catkin_ws && \
    rosdep update && apt-get update && \
    rosdep install --from-paths src --ignore-src -r -y && \
    catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/ros/noetic install &&  \
    rm -r /tmp/catkin_ws

# Install tmux 3.2(if you wish)
RUN apt-get update && apt-get install -y automake autoconf pkg-config libevent-dev libncurses5-dev bison && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/tmux/tmux.git && \
cd tmux && git checkout tags/3.2 && ls -la && sh autogen.sh && ./configure && make -j8 && make install

ARG ROBOT_NAME

# Add user and group
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD
ARG NETWORK_IF
ARG WORKSPACE_DIR
ARG DOCKER_DIR
RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}
#RUN sudo rosdep init && rosdep update
#RUN sudo rosdep init && rosdep update

RUN cd ~/ &&  \
    git config --global user.email "you@example.con" &&  \
    git config --global user.name "Your Name"

# create catkin_ws
RUN mkdir -p /home/$USER_NAME/catkin_ws/src
RUN cd /home/$USER_NAME/catkin_ws/src && /bin/bash -c '. /opt/ros/noetic/setup.bash; catkin_init_workspace'
RUN /bin/bash -c '. /opt/ros/noetic/setup.bash; cd ~/catkin_ws; catkin_make'
      
# Terminator
RUN mkdir -p ~/.config/terminator/
COPY assets/terminator_config /home/$USER_NAME/.config/terminator/config 
COPY assets/entrypoint.sh /entrypoint.sh

# .bashrc
RUN echo "export ROS_HOME=~/.ros" >> ~/.bashrc
RUN echo "export ROBOT_NAME=${ROBOT_NAME}" >> ~/.bashrc
RUN echo "export WORKSPACE_DIR=${WORKSPACE_DIR}" >> ~/.bashrc
RUN echo "alias sim_mode='export ROS_HOSTNAME=localhost ROS_MASTER_URI=http://localhost:11311 export PS1=\"\[\033[44;1;37m\]<local>\[\033[0m\]\w$ \"'" >> ~/.bashrc
RUN echo "export ROS_MASTER_URI=http://${ROBOT_NAME}.local:11311" >> ~/.bashrc
RUN echo "export PS1=\"\[\033[44;1;37m\]<sciurus>\[\033[0m\]\w$ \"" >> ~/.bashrc
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
RUN echo "export MESA_LOADER_DRIVER_OVERRIDE=i965" >> ~/.bashrc

COPY ./assets/entrypoint.sh /
RUN sudo chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["terminator"]

