# Sciurus17 development environment with Docker
Sciurus17 can now be developed even if the host PC is running Ubuntu22.04.

## Getting started
### Setup on host PC
Specification
- Ubuntu22.04.3 LTS
- Docker version 24.0.7, build afdd53b
- Docker Copmose version v2.21.0

#### Install Docker Engine
#### NVIDIA(GPU) driver installation
#### Install this Repository
```
(host)$ cd ~
(host)$ git clone https://gitlab.com/tidbots/sciurus17/sciurus17-noetic_docker.git
(host)$ cd sciurus17-noetic_docker
```

#### Edit configuration file
Edit the .env file to match your environment.

```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ cat .env

# Copyright (c) 2024 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
USER_NAME=<USER NAME>
GROUP_NAME=<GROUP NAME>
UID=1000
GID=1000
PASSWORD=<Password>
NETWORK_IF=enx245ebe7f410b
ROBOT_NAME=hsrc28
WORKSPACE_DIR=/home/<YOUR HOME>/share
ROS_IP=192.168.1.47
ROBOT_NAME=sciurus17
```

- NETWORK_IF should be set to the network adapter name of the host PC<br>
Network adapter can be found with the `ip -a`  command

```
(host)$ ip a

```

- ROBOT_NAME should be set to the host　name of Sciurus17．


- WORKSPACE_DIR must be set to the absolute path of the directory to be shared between the host PC and the container.

Any directory creation, file editing, package installation, etc. done in the Docker container will not be reflected in the Docker image, but will disappear when the container is destroyed.
This is the relationship between a class (Docker image) and an instance (Docker container) in object-oriented terms.

So, mount and share the host PC's directory with the container.
In this way, editing work in mutually mounted directories will not disappear.

### Creating a directory to be shared between the host computer and the container

Before launching the container for the first time, please create a directory on the host computer as shown below as a preliminary preparation.

```
(host)$ cd ~
(host)$ mkdir share
```
If you do not create a shared directory in advance, the Docker system will create it automatically.
However, the automatically created shared directory will be created with root privileges, which means that you will not be able to freely write to the directory.
In such a case, after stopping the container, change the user and group of the directory you want to share on the host computer as shown below.

```
(host)$ cd ~
(host)$ ls -al share
合計 8
drwxr-xr-x  2 root   roor     4096 12月 31 23:29 .
drwxr-x--- 24 <User Name> <Group name> 4096 12月 31 23:29 ..
```
```
$ sudo chown <User Name> share/
$ sudo chgrp <Group Name> share/
```

Verify that the user and group names in the shared directory have been changed.
```
$ ls -al share
合計 8
drwxr-xr-x  2 <User Name> <Group Name> 4096 12月 31 23:31 .
drwxr-x--- 24 <User name> <Group Name> 4096 12月 31 23:31 ..
```

### Building a Docker Image
```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ docker compose build
```
Docker image will be created named `okdhryk/sciurus`.
If you want to change the image name, edit compose.yml.


## Developing in the Simulator
### Creating Docker Containers
On the host PC, start the container with the following command.
```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ docker compose up
```
To enter a running container from another terminal on the host computer, execute the following command.
```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ docker compose exec sciurus-dev /bin/bash
```

### Starting the Sciurus17 Simulator
```
(container)$ sim_mode
(container)$ roslaunch sciurus17_gazebo sciurus17_with_table.launch
```

<img src="images/Screenshot_from_2024-07-07_13-30-43.png" width=256>

<img src="images/Screenshot_from_2024-07-07_13-30-58.png" width=256>




## Develop with the robot
### Creating Docker Containers
On the host PC, start the container with the following command.
```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ docker compose up
```
To enter a running container from another terminal on the host computer, execute the following command.
```
(host)$ cd ~/sciurus17-noetic_docker
(host)$ docker compose exec sciurus-dev /bin/bash
```

### Cnnection to actual robot
```
(container)$ ping sciurus17.local
...
...
```

### Check ROS environment variables
Check that the environment variables related to ROS are set correctly.　
```
(container)$ env | grep ROS
ROS_MASTER_URI=http://sciurus17.local:11311
ROS_IP=192.168.190.20
...
...

```

### Try the sample program (RT official)
#### gripper_action_example
```
(container)$ rosrun sciurus17_examples gripper_action_example.py
```
#### neck_joint_trajectory_example
```
(container)$ rosrun sciurus17_examples neck_joint_trajectory_example.py
```
#### waist_joint_trajectory_example
```
(container)$ waist_joint_trajectory_example
```
#### pick_and_place_demo
```
(container)$ rosrun sciurus17_examples pick_and_place_right_arm_demo.py
```
#### hand_position_publisher
```
(container)$ rosrun sciurus17_examples hand_position_publisher_example.py
```
#### head_camera_tracking
```
(container)$ rosrun sciurus17_examples head_camera_tracking.py
```

##### ボール追跡をする場合

./scripts/head_camera_tracking.pyを編集します。
```
def _image_callback(self, ros_image):
    # ~~~ 省略 ~~~

        # オブジェクト(特定色 or 顔) の検出
        output_image = self._detect_orange_object(input_image)
        # output_image = self._detect_blue_object(input_image)
        # output_image = self._detect_face(input_image)
```
##### 顔追跡をする場合
./scripts/head_camera_tracking.pyを編集します。

顔追跡にはカスケード型分類器を使用します。

カスケードファイルのディレクトリを設定してください。 USER_NAME は環境に合わせて書き換えてください。
```
class ObjectTracker:
    def __init__(self):
        # ~~~ 省略 ~~~

        # カスケードファイルの読み込み
        # 例
        # self._face_cascade = cv2.CascadeClassifier("/home/USER_NAME/.local/lib/python2.7/site-packages/cv2/data/haarcascade_frontalface_alt2.xml")
        # self._eyes_cascade = cv2.CascadeClassifier("/home/USER_NAME/.local/lib/python2.7/site-packages/cv2/data/haarcascade_eye.xml")
        self._face_cascade = cv2.CascadeClassifier("/home/USER_NAME/.local/lib/python2.7/site-packages/cv2/data/haarcascade_frontalface_alt2.xml")
        self._eyes_cascade = cv2.CascadeClassifier("/home/USER_NAME/.local/lib/python2.7/site-packages/cv2/data/haarcascade_eye.xml")
```
```
def _image_callback(self, ros_image):
    # ~~~ 省略 ~~~

        # オブジェクト(特定色 or 顔) の検出
        # output_image = self._detect_orange_object(input_image)
        # output_image = self._detect_blue_object(input_image)
        output_image = self._detect_face(input_image)
```

#### chest_camera_tracking
```
(container)$ rosrun sciurus17_examples chest_camera_tracking.py
```
#### depth_camera_tracking
```
(container)$ rosrun sciurus17_examples depth_camera_tracking.py
```

デフォルトでは検出範囲を4段階に分けています。 検出範囲を変更する場合は./scripts/depth_camera_tracking.pyを編集します。
```
    def _detect_object(self, input_depth_image):
        # 検出するオブジェクトの大きさを制限する
        MIN_OBJECT_SIZE = 10000 # px * px
        MAX_OBJECT_SIZE = 80000 # px * px

        # 検出範囲を4段階設ける
        # 単位はmm
        DETECTION_DEPTH = [
                (500, 700),
                (600, 800),
                (700, 900),
                (800, 1000)]
```                

#### preset_pid_gain_example
```
(container)$ roslaunch sciurus17_examples preset_pid_gain_example.launch
```
#### box_stacking_example
```
(container)$ roslaunch sciurus17_examples box_stacking_example.launch
```

#### current_control_right_arm
```
(container)$ 
```
#### current_control_left_wrist
```
(container)$ a
```